# Weirdness

This is a Kosmo format synth module based on the Music From Outer Space [Weird Sound Generator](https://musicfromouterspace.com/index.php?MAINTAB=SYNTHDIY&PROJARG=WSG2010/wsg_page1.html&CATPARTNO=PCBMFWSGNNONE01&PN=1&SONGID=NONE) by Ray Wilson.

It incorporates the two three-oscillator voices (Zany, Wacky, and Weird) from the WSG, essentially unchanged (aside from using +12 V instead of +9 V for power).

The Oddness low pass filter is omitted. After all, you almost certainly have a low pass filter module already. In its place there is a mixing buffer for each voice. 

Additionally, there is an indicator LED and gate output for each Zany oscillator.

An optional pin header on the PCB will allow normal connections to another module if I ever get around to designing one.

## Build notes

Wilson specifies linear taper for all the pots. I think audio taper makes more sense for the 1M frequency pots, but linear will work.

There are holes in the front panel for two M2 screws with spacers to go into the rightmost slide pots' screw holes and support that side of the front PCB. This is probably not essential. If used, the holes may interfere with slide pot shaft caps unless there is some clearance between the caps and the panel. In my build, getting that clearance required putting the front PCB closer to the panel by omitting any nuts on the toggle switches behind the panel — the panel is directly on top of the toggle switch housings. The film caps I used on the front PCB were the same height as the switch housings; if they were taller, I might have had to mount them horizontally or perhaps behind the PCB.

## Current draw
20 mA +12 V, 4 mA -12 V


## Photos

![](Images/wsgmodule.jpg)

<!-- ![]() -->

## Documentation

* [Schematic](Docs/wsgmodule_schematic.pdf)
* PCB layout: 
  * Main PCB: [front](Docs/Layout/wsgmodule_mainPCB/wsgmodule_mainPCB_F.SilkS,F.Mask.pdf), [back](Docs/Layout/wsgmodule_mainPCB/wsgmodule_mainPCB_B.SilkS,B.Mask.pdf)
  * Panel components PCB: [front](Docs/Layout/wsgmodule_panelCompsPCB/wsgmodule_panelCompsPCB_F.SilkS,F.Mask.pdf), [back](Docs/Layout/wsgmodule_panelCompsPCB/wsgmodule_panelCompsPCB_B.SilkS,B.Mask.pdf)
* [BOM](Docs/BOM/wsgmodule_bom.md)
* [Blog post](https://www.richholmes.xyz/analogoutput/2024-04-16_weirdness/)

## Git repository

* [https://gitlab.com/rsholmes/wsgmodule](https://gitlab.com/rsholmes/wsgmodule)

