# wsgmodule.kicad_sch BOM

Mon 15 Apr 2024 09:00:08 PM EDT

Generated from schematic by Eeschema 7.0.11-7.0.11~ubuntu22.04.1

**Component Count:** 99


## 
| Refs | Qty | Component | Description | Vendor | SKU | Note |
| ----- | --- | ---- | ----------- | ---- | ---- | ---- |
| C4, C5, C12, C13 | 4 | 22nF | Polyester film capacitor, 5 mm pitch | Tayda |  |  |
| C7, C14–16, C105–108 | 8 | 1uF | Polyester film capacitor, 5 mm pitch | Tayda |  |  |
| C9, C103, C104 | 3 | 100nF | Ceramic capacitor, 2.5 mm pitch | Tayda | A-553 |  |
| C101, C102 | 2 | 10uF | Electrolytic capacitor, 2.5 mm pitch | Tayda | A-4349 |  |
| D1, D2 | 2 | 1N4148 | Standard switching diode, DO-35 | Tayda | A-157 |  |
| D101, D102 | 2 | 1N5817 | Schottky Barrier Rectifier Diode, DO-41 | Tayda | A-159 |  |
| D103, D104 | 2 | LED_green | Light emitting diode | Tayda | A-1553 |  |
| J101 | 1 | Synth_power_2x5 | Pin header 2.54 mm 2x5 | Tayda | A-2939 |  |
| J102 | 1 | Conn_01x03_Pin | 3 position pin header, 2.54 mm pitch |  |  |  |
| J103, J112 | 2 | Conn_01x10_Socket | Generic connector, single row, 01x10, script generated |  |  |  |
| J104, J105, J107, J110 | 4 | AudioJack2 | Audio Jack, 2 Poles (Mono / TS) | Tayda | A-1121 |  |
| J106, J111 | 2 | Conn_01x10_Pin | Generic connector, single row, 01x10, script generated |  |  |  |
| J108 | 1 | DIP-14 | 14 pin DIP socket | Tayda | A-004 |  |
| J109 | 1 | DIP-8 | 8 pin DIP socket | Tayda | A-001 |  |
| Q1, Q2, Q101, Q102 | 4 | 2N3904 | Small Signal NPN Transistor, TO-92 | Tayda | A-111 |  |
| R2, R5, R21, R22, R105, R111 | 6 | 100k | Resistor | Tayda |  |  |
| R6, R7, R14, R15, R23, R24, R27, R28 | 8 | 4.7k | Resistor | Tayda |  |  |
| R19, R30, R33, R34, R103, R109 | 6 | 47k | Resistor | Tayda |  |  |
| R101, R107 | 2 | RL_GREEN | Resistor | Tayda |  |  |
| R102, R104, R106, R108, R110, R112 | 6 | 1k | Resistor | Tayda |  |  |
| RV1, RV9, RV13, RV20, RV25, RV26 | 6 | A1M | 45 mm slide pot | Mouser | 652-PTA4553215DPA105 | See notes |
| RV18, RV29 | 2 | B100k | 45 mm slide pot | Mouser | 652-PTA45532015DPA10 |  |
| SW1, SW2, SW4–7 | 6 | SW_SPST | SPST toggle switch | Tayda | A-3186 (SPDT, use as SPST) |  |
| U1 | 1 | 40106 | Hex Schmitt trigger inverter |  |  |  |
| U101 | 1 | TL072 | Dual operational amplifier, DIP-8 | Tayda | A-037 |  |
| ZKN101–108 | 8 | Knob_MF-A01 | Slide pot cap |  |  |  |
| ZSC101, ZSC102 | 2 | Screw | M2 6? mm screw |  |  |  |
| ZWA101–106 | 6 | Washer | M2 washer |  |  |  |

### Resistors in color band order
|Value|Qty|Refs|
|----|----|----|
|1k|6|R102, R104, R106, R108, R110, R112|
|100k|6|R2, R5, R21, R22, R105, R111|
|4.7k|8|R6, R7, R14, R15, R23, R24, R27, R28|
|47k|6|R19, R30, R33, R34, R103, R109|
|RL_GREEN|2|R101, R107|

